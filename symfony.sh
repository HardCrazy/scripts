if [ "$EUID" -ne 0 ]
then
    sudo su root "$0"
    exit
fi

CAN_I_RUN_SUDO=$(sudo -n uptime 2>&1|grep "load"|wc -l)

if [ ${CAN_I_RUN_SUDO} -gt 0 ]
then
    echo "Running script ..."
else
    echo "Please check if you have access to root"
    exit
fi

COMPOSER='php /home/shared/composer.phar'
RED='\033[0;31m'
NC='\033[0m'
USER='ws'
CONSOLE='bin/console'
USER_CONSOLE='sudo -u ${USER} php ${CONSOLE}'


sudo -u $USER $COMPOSER install
eval $USER_CONSOLE --version

SCHEMA_UP_TO_DATE=$(eval $USER_CONSOLE doctrine:schema:update --dump-sql)
if [ "$SCHEMA_UP_TO_DATE" != "Nothing to update - your database is already in sync with the current entity metadata." ]
then
    echo -e "${RED}The database schema is not up to date. To update it please run the following command : php bin/console doctrine:schema:update --dump-sql --force${NC}"
    exit
fi

php bin/console cache:clear --env=prod --no-warmup
php bin/console cache:clear --env=dev --no-warmup

eval $USER_CONSOLE assets:install

eval $USER_CONSOLE cache:warmup --env=prod
eval $USER_CONSOLE cache:warmup --env=dev

chmod -R 777 var/logs var/sessions var/cache app/config/parameters.yml web/uploads
chmod -R 755 web/images
